<?php

namespace App;

use App\Book;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','child_of'];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
