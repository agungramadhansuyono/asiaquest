<?php

namespace App;

use App\Category;
use App\Keywords;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'name',
        'description',
        // 'category',
        // 'keywords',
        'price',
        'stock',
        'publisher'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'book_category');
    }

    public function keywords()
    {
        return $this->belongsToMany(Keywords::class, 'book_keyword');
    }
}
