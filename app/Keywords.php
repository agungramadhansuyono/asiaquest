<?php

namespace App;

use App\Book;
use Illuminate\Database\Eloquent\Model;

class Keywords extends Model
{
    protected $fillable = ['name'];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
