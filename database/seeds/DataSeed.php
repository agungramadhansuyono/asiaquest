<?php

use App\Book;
use App\Category;
use App\Keywords;
use Illuminate\Database\Seeder;

class DataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 

        Category::create([
            'name' => 'umum',
            'child_of' => 0
        ]);
        
        Category::create([
            'name' => 'interaksi keluarga',
            'child_of' => 1
        ]);
        
        Category::create([
            'name' => 'remaja',
            'child_of' => 1
        ]);
        
        Keywords::create(['name' => 'kepemimpinan']);
        Keywords::create(['name' => 'seni']);
        Keywords::create(['name' => 'berbicara']);
        Keywords::create(['name' => 'remaja']);
        Keywords::create(['name' => 'tantangan']);
        Keywords::create(['name' => 'prinsip']);
        Keywords::create(['name' => 'keputusan']);

        for ($i=0; $i < 10; $i++) { 
            $book = Book::create([
                'name' => $faker->word(),
                'description' => '',
                // 'category_id' => $faker->randomElements([1,2,3]),
                // 'keyword_id' => $faker->randomElements([1,2,3,4,5]),
                'price' => number_format('100000'),
                'stock' => 10,
                'publisher' => ''
            ]);

            $book->categories()->attach($faker->randomElements([1,2,3]));
            $book->keywords()->attach($faker->randomElements([1,2,3]));
        }
    }
}
